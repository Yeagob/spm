﻿using UnityEngine;
using System.Collections;

public class PlayerSPController : MonoBehaviour {
	public StreamReader spReader;
	public UIStepController uiSteps;

	public int matrixRows, matrixCols;

	Animator anim;
	bool ground = false;

	public int maxWeightFootValue = 50, minWeightFootValue = 2;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		WalkSystem ();


	}

	bool WalkSystem () {
		float[] magnitudes = new float[2];

		//Getting magnitudes from stream reader
		magnitudes = spReader.GetMagnitudes (2);

		//Debug
		print ("Magnitud pisada izquierda: " + magnitudes [0] + " / Magnitud pisada derecha: " + magnitudes [1]);

		//Interface steps
		uiSteps.SetImageValues(magnitudes, maxWeightFootValue);

		//Jump
//		if (ground && magnitudes [0] < minWeightFootValue && magnitudes [1] < minWeightFootValue ) {
//			ground = false;
//			anim.SetTrigger ("Jump");
//			return true;
//		}

		//Landing
		if (!ground && magnitudes [0] > 50 && magnitudes [1] > 50) {
			ground = true;
		}


		//RigthDown
		if (magnitudes [1] > maxWeightFootValue){
			anim.SetBool ("RigthUp", false);
			//return true;
		}

		//RigthUp
		if (magnitudes [1] < minWeightFootValue){
			anim.SetBool ("RigthUp", true);
			//return true;
		}

	

		//LeftUp
		if (magnitudes [0] < minWeightFootValue) {
			anim.SetBool ("LeftUp", true);
			//return true;
		}

		//LeftDown
		if (magnitudes [0] > maxWeightFootValue){
			anim.SetBool ("LeftUp", false);
			//return true;
		}




		return false;
	}
}
